FROM alpine:3.4

MAINTAINER Paul Jang @qgp9
# Thanks to Irakli Nadareishvili @inadarei

ADD Gemfile .
RUN apk upgrade --update \
 && apk add libatomic readline readline-dev libxml2 libxml2-dev \
        ncurses-terminfo-base ncurses-terminfo \
        libxslt libxslt-dev zlib-dev zlib \
        ruby ruby-dev ruby-rdoc ruby-irb \
        yaml yaml-dev \
        libffi-dev build-base git nodejs \
 && apk add lftp \
 && apk add bash \
 && apk add rsync \
 && apk add perl \
 && apk add imagemagick \
 && apk add openssh-client \
 && gem install --no-document rake \
 && gem install --no-document bundler \
 && gem install --no-document io-console \
 && bundler install --system \
 && rm -rf /root/src /tmp/* /usr/share/man  \
 && apk del build-base zlib-dev readline-dev \
            yaml-dev libffi-dev libxml2-dev \
 && apk search --update
 
# && apk cache clean \ 
# rack rack-rewrite rack-contrib puma
# && gem install --no-document rubygems-bundler \
# && gem regenerate_binstubs \
 
